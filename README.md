# SHashMap
Project name: SHashMap  
Author: fenrir

Description: Naive implementation of a synchronized HashMap using POSIX mutex 
data structures, and that has no collision detection, overwriting values of the 
same bucket and being NUMA-aware.