#include <stdio.h>
#include <stdlib.h>
#include "shashmap.h"

int32_t shashmap_default_hash(shashmap *hashmap, int32_t val)
{
    return val % hashmap->size;
}

int shashmap_init_default(shashmap *hashmap, int32_t size)
{
    int i, rc;
    
    if (size <= 0) {
        printf("ERROR: hashmap must have a size greater than 0!\n");
        return SHASHMAP_RET_ERR;
    }
    hashmap->size = size;

    hashmap->hash = shashmap_default_hash;

    hashmap->map = (sentry*) malloc(sizeof(sentry) * size);
    if (hashmap->map == NULL) {
        printf("ERROR: could not allocated hashmap!\n");
        return SHASHMAP_RET_ERR;
    }

    for (i = 0; i < size; i++) {
        rc = pthread_mutex_init(&(hashmap->map[i].mutex), NULL);
        if (rc) {
            printf("ERROR: could not initialize a mutex!\n");
            return SHASHMAP_RET_ERR;
        }
    }

    return SHASHMAP_RET_OK;
}

int shashmap_init(shashmap *hashmap, int32_t size,
        int32_t (*hash)(shashmap*, int32_t))
{
    int i, rc;

    if (size <= 0) {
        printf("ERROR: hashmap must have a size greater than 0!\n");
        return SHASHMAP_RET_ERR;
    }
    hashmap->size = size;

    hashmap->hash = hash;

    hashmap->map = (sentry*) malloc(sizeof(sentry) * size);
    if (hashmap->map == NULL) {
        printf("ERROR: could not allocated hashmap!\n");
        return SHASHMAP_RET_ERR;
    }

    for (i = 0; i < size; i++) {
        rc = pthread_mutex_init(&(hashmap->map[i].mutex), NULL);
        if (rc) {
            printf("ERROR: could not initialize a mutex!\n");
            return SHASHMAP_RET_ERR;
        }
    }

    return SHASHMAP_RET_OK;
}


int shashmap_put(shashmap *hashmap, int32_t key, int32_t val)
{
    int k;

    k = hashmap->hash(hashmap, key);
    pthread_mutex_lock(&(hashmap->map[k].mutex));
    hashmap->map[k].value = val;
    pthread_mutex_unlock(&(hashmap->map[k].mutex));

    return SHASHMAP_RET_OK;
}

int shashmap_get(shashmap *hashmap, int32_t key, int32_t *val)
{
    int k;

    k = hashmap->hash(hashmap, key);
    *val = hashmap->map[k].value;

    return SHASHMAP_RET_OK;
}

int shashmap_destroy(shashmap *hashmap)
{
    int i, rc;

    for (i = 0; i < hashmap->size; i++) {
        rc = pthread_mutex_destroy(&(hashmap->map[i].mutex));
        if (rc) {
            printf("ERROR: could not destroy a mutex!\n");
            return SHASHMAP_RET_ERR;
        }
    }
    hashmap->size = -1;
    hashmap->hash = shashmap_default_hash;
    free(hashmap->map);
    hashmap->map = 0;

    return SHASHMAP_RET_OK;
}
