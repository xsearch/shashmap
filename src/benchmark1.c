#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include "shashmap.h"

#define DEBUG 0
#define HELP "Required parameters: <seed> <size> <num elem> <num threads>\n"

typedef struct thread_arg {
    int tid;
    int32_t num_elems;
    int32_t *elems;
    shashmap *hashmap;
    pthread_barrier_t *barrier;
    int rc;
} thread_arg;

void *work(void *param)
{
    int i, rc;
    thread_arg *arg;
    shashmap *hashmap;
    int32_t *elems;
    int32_t num_elems;

    arg = (thread_arg *) param;
    arg->rc = 0;
    num_elems = arg->num_elems;

    // allocate memory for the array of elements
    hashmap = arg->hashmap;

    elems = (int32_t*) malloc(sizeof(int32_t) * num_elems);
    if (elems == NULL) { 
        printf("ERROR: thread %d could not allocate elements!\n", arg->tid);
        arg->rc = 1;
    }
    arg->elems = elems;
    // complete barrier in order to allow main thread to do initialization
    pthread_barrier_wait(arg->barrier);

    // wait for main thread to initialize the array of elements
    pthread_barrier_wait(arg->barrier);

    // put elements in the hashmap
    if (arg->rc) {
        pthread_exit(NULL);
    }

    for (i = 0; i < num_elems; i++) {
        rc = shashmap_put(hashmap, elems[i], elems[i]);
        if (rc != SHASHMAP_RET_OK) {
            arg->rc = 2;
        }
    }

    pthread_exit(NULL);
}


void benchmark(int32_t seed, int32_t size, int32_t num_elems, int num_threads)
{
    int i, j, rc, rc_agg, val;
    shashmap *hashmap;
    pthread_t threads[num_threads];
    pthread_barrier_t *barrier;
    thread_arg args[num_threads];
    struct timeval put_tik, put_tok, get_tik, get_tok;
    double put_time, get_time;
    
    // allocate barrier
    barrier = (pthread_barrier_t*) malloc(sizeof(pthread_barrier_t));
    if (barrier == NULL) {
        printf("ERROR: could not allocate barrier!\n");
        return;
    }

    // initialize barrier
    rc = pthread_barrier_init(barrier, NULL, num_threads + 1);
    if (rc) {
        printf("ERROR: could not initialize barrier!\n");
        return;
    }

    // allocate hashmap
    hashmap = (shashmap*) malloc(sizeof(shashmap));
    if (hashmap == NULL) {
        printf("ERROR: could not allocate the hashmap!\n");
        return;
    }
    
    // initialize hashmap
    rc = shashmap_init_default(hashmap, size);
    if (rc != SHASHMAP_RET_OK) {
        printf("ERROR: could not initialize the hashmap!\n");
        return;
    }

    // initialize thread arguments
    if (DEBUG) {
        printf("INFO: initializing and creating threads\n");
    }
    for (i = 0; i < num_threads; i++) {
        args[i].tid = i;
        args[i].num_elems = num_elems / num_threads;
        args[i].hashmap = hashmap;
        args[i].barrier = barrier;
    }

    // create threads
    for (i = 0; i < num_threads; i++) {
        rc = pthread_create(&threads[i], NULL, work, &args[i]);
        if (rc) {
            printf("ERROR: could not create thread %d!\n", i);
        }
    }

    // wait for threads to allocate memory for the array of elements
    pthread_barrier_wait(barrier);

    // initialize the array of elements of each thread
    if (DEBUG) {
        printf("INFO: initializing the array of elements for each thread\n");
    }
    srand(seed);
    for (i = 0; i < num_threads; i++) {
        for (j = 0; j < (num_elems / num_threads); j++) {
            args[i].elems[j] = rand() % (size / 2) + 1;
        }
    }

    if (DEBUG) {
        printf("INFO: started running put and get operations\n");
    }
    // complete barrier that allows the threads to continue execution
    pthread_barrier_wait(barrier);

    // start measuring time
    gettimeofday(&put_tik, NULL);

    // join threads while measuring time
    rc_agg = 0;
    for (i = 0; i < num_threads; i++) {
        rc = pthread_join(threads[i], NULL);
        if (rc) {
            printf("ERROR: could not join thread %d!\n", i);
        }
        if (args[i].rc) {
            rc_agg = 1;
        }
    }

    // stop measuring time
    gettimeofday(&put_tok, NULL);
    
    // calculate and print put throughput
    put_time = ((double) put_tok.tv_sec - (double) put_tik.tv_sec) * 1000000 
            + ((double) put_tok.tv_usec - (double) put_tik.tv_usec);
    printf("RESULTS: put throughput = %lf OPS\n",
            (double) num_elems / (put_time / 1000000));
    if (rc_agg) {
        printf("WARN: the hashmap put is buggy!\n");
    }

    // start measuting time
    gettimeofday(&get_tik, NULL);
    
    // Measure the time it takes to lookup all the elements
    rc_agg = 0;
    for (i = 0; i < num_threads; i++) {
        for (j = 0; j < (num_elems / num_threads); j++) {
            val = 0;
            rc = shashmap_get(hashmap, args[i].elems[j], &val);
            if (rc != SHASHMAP_RET_OK) {
                rc_agg = 1;
            }
            if (val == 0) {
                rc_agg = 1;
            }
        }
    }

    // stop measuring time
    gettimeofday(&get_tok, NULL);
    
    // calculate and print get throughput
    get_time = ((double) get_tok.tv_sec - (double) get_tik.tv_sec) * 1000000 
            + ((double) get_tok.tv_usec - (double) get_tik.tv_usec);
    printf("RESULTS: get throughput = %lf OPS\n",
            (double) num_elems / (get_time / 1000000));
    if (rc_agg) {
        printf("WARN: the hashmap get is buggy!\n");
    }
    
    // destroy barrier and free memory
    pthread_barrier_destroy(barrier);
    free(barrier);
    shashmap_destroy(hashmap);
    free(hashmap);
    for (i = 0; i < num_threads; i++) {
        free(args[i].elems);
    }
}

int main(int argc, char *argv[argc])
{
    int num_threads;
    int32_t seed, size, num_elems;

    // check for right number of program arguments
    if (argc != 5) {
        printf(HELP);
        return 0;
    }

    // extract program arguments
    seed = (int32_t) atoi(argv[1]);
    size = (int32_t) atoi(argv[2]);
    num_elems = (int32_t) atoi(argv[3]);
    num_threads = atoi(argv[4]);

    // start the benchmark
    printf("Running the SHashMap benchmark #1 ...\n");
    benchmark(seed, 1 << size, 1 << num_elems, num_threads);
    printf("Finished running the benchmark.\n");

    return 0;
}
