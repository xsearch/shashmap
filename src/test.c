#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "shashmap.h"

#define TOTAL_TESTS 2
#define SEED 11155
#define SIZE 1 << 28
#define NUMVALS 1 << 20

void init_vals(int32_t vals[NUMVALS])
{
    int i;
    
    srand(SEED);
    for (i = 0; i < NUMVALS; i++) {
        vals[i] = rand() % (SIZE / 2);
    }
}

int test_put_vals(shashmap *hashmap, int32_t vals[NUMVALS])
{
    int i, rc;

    for (i = 0; i < NUMVALS; i++) {
        rc = shashmap_put(hashmap, vals[i], vals[i]);
        if (rc != SHASHMAP_RET_OK) {
            printf("BUG: shashmap_put returned %d, while expecting %d!\n",
                    rc, SHASHMAP_RET_OK);
            return 1;
        }
    }

    return 0;
}

int test_get_vals(shashmap *hashmap, int32_t vals[NUMVALS])
{
    int i, rc, val;

    for (i = 0; i < NUMVALS; i++) {
        val = 0;
        rc = shashmap_get(hashmap, vals[i], &val);
        if (rc != SHASHMAP_RET_OK) {
            printf("BUG: shashmap_get returned %d, while expecting %d!\n",
                    rc, SHASHMAP_RET_OK);
            return 1;
        }
        if (val != vals[i]) {
            printf("BUG: shashmap_get got %d, while expecting %d!\n",
                    val, vals[i]);
            return 1;
        }
    }

    return 0;
}

int test_default(const char teststr[])
{
    int rc;
    int32_t *vals;
    shashmap hashmap;

    printf("%s\n", teststr);
    vals = (int32_t*) malloc(sizeof(int32_t) * NUMVALS);
    init_vals(vals);
    
    rc = shashmap_init_default(&hashmap, SIZE); 
    if (rc != SHASHMAP_RET_OK) {
        printf("BUG: shashmap_init returned %d, while expecting %d!\n",
                rc, SHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: shashmap_init successful.\n");

    rc = test_put_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: shashmap_put successful.\n");

    rc = test_get_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: shashmap_get successful.\n");

    rc = shashmap_destroy(&hashmap);
    if (rc != SHASHMAP_RET_OK) {
        printf("BUG: shashmap_destroy returned %d, while expecting %d!\n",
                rc, SHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: shashmap_destroy successful.\n");

    free(vals);
    printf("PASS: test_custom passed!\n");
    return 1;
}

int32_t test_hash(shashmap *hashmap, int32_t val)
{
    return val % (hashmap->size / 2);
}

int test_custom(const char teststr[])
{
    int rc;
    int32_t *vals;
    shashmap hashmap;

    printf("%s\n", teststr);
    vals = (int32_t*) malloc(sizeof(int32_t) * NUMVALS);
    init_vals(vals);
    
    rc = shashmap_init(&hashmap, SIZE, test_hash); 
    if (rc != SHASHMAP_RET_OK) {
        printf("BUG: shashmap_init returned %d, while expecting %d!\n",
                rc, SHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: shashmap_init successful.\n");

    rc = test_put_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: shashmap_put successful.\n");

    rc = test_get_vals(&hashmap, vals);
    if (rc != 0) {
        return 0;
    }
    printf("INFO: shashmap_get successful.\n");

    rc = shashmap_destroy(&hashmap);
    if (rc != SHASHMAP_RET_OK) {
        printf("BUG: shashmap_destroy returned %d, while expecting %d!\n",
                rc, SHASHMAP_RET_OK);
        return 0;
    }
    printf("INFO: shashmap_destroy successful.\n");

    free(vals);
    printf("PASS: test_custom passed!\n");
    return 1;
}

int main(int argc, char *argv[argc])
{
    int num_tests = 0;

    printf("Running SHashMap tests ...\n");
    num_tests += test_default("Testing hashmap with default hash function.");
    num_tests += test_custom("Testing hashmap with custom hash function.");
    printf("Finished running tests. %d/%d tests have passed.\n", 
            num_tests, TOTAL_TESTS);

    return 0;
}
