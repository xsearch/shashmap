#ifndef SHASHMAP_H
#define SHASHMAP_H

#include <stdint.h>
#include <pthread.h>

#define SHASHMAP_RET_OK 0
#define SHASHMAP_RET_ERR -1

typedef struct sentry {
    int32_t value;
    pthread_mutex_t mutex;
} sentry;

typedef struct shashmap {
    sentry *map;
    int32_t size;
    int32_t (*hash)(struct shashmap*, int32_t);
} shashmap;

extern int shashmap_init_default(shashmap *hashmap, int32_t size);
extern int shashmap_init(shashmap *hashmap, int32_t size,
        int32_t (*hash)(shashmap*, int32_t));
extern int shashmap_put(shashmap *hashmap, int32_t key, int32_t val);
extern int shashmap_get(shashmap *hashmap, int32_t key, int32_t *val);
extern int shashmap_destroy(shashmap *hashmap);

#endif
